/*
 * Developed by Marius Stuparu for Siveco Romania SA
 * Copyright (c) 2011 Siveco Romania SA http://www.siveco.ro
 * 
 * Version 1.0
 * 
 * This needs tha ipadTouchDrag.js plugin in order to work
 * 
 * You can style its appearance using ipadSliderInput.css
 * 
 * Feel free to modify it to suit your needs, but do not delete this legal warning,
 * of if you minify automatically, please put a reference somewhere else.
 * 
 * Usage:
 * 1. Include the 3 required scripts in your HTML file, preferably just before the </body> tag,
 * but not after your main script:
 * <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.js"></script>
 * <script src="js/ipadTouchDrag.js"></script>
 * <script src="js/ipadSliderInput.js"></script>
 * 
 * 2. Include the stylesheet file in the <head> (you can customise or add themes in it)
 * <link rel="stylesheet" href="css/ipadSliderInput.css">
 * 
 * 3. From your main script file, create a new ipadSliderInput object like this:
 * whatever = new ipadSliderInput('[container element]', {
		steps: 10,
		defaultVal: 50 // more options below
	}, function callBack(value){
		// do whatever with the value returned here
	})
 * 
 * there is also a public method, if you want to move the slider programatically:
 * whatever.moveHandleTo(newValue)
 * 
 */
;(function($) {
	
    ipadSliderInput = function(el, options, callBack) {

        // plugin's default options
        var defaults = {
            direction: 'horizontal', // possible values are 'horizontal' and 'vertical'
			defaultVal: 0, // slider initial value
			minVal: 0, // lowest value
			maxVal: 100, // highest value
			values: 'percent', // possible values are 'percent', 'numeric'
			steps: 0, // if set to 0, can take any value between minVal and maxVal
			theme: 'sliderInputDefault', // the slider appearance - see ipadSliderInput.css
			displayTicks: true,
			displayTooltip: true
        }
        
        // needs this plugin really bad. if not loaded, no joy!
        if (!ipadTouchDrag){
			console.error('ERROR! This plugin needs ipadTouchDrag.js. Please add it in your page BEFORE ipadSliderInput is used.');
			return false;
		}
        
        // validate options
		if((typeof options.direction) != 'string') { options.direction = 'horizontal' }
		switch (options.direction){
			case 'horizontal':
			case 'vertical': break;
			default: options.direction = 'horizontal'; break;
		}
		
		if((typeof options.defaultVal) == 'string') { options.defaultVal = parseInt(options.defaultVal) }
		if((typeof options.defaultVal) != 'number') { options.defaultVal = 0 }
		
		if((typeof options.minVal) == 'string') { options.minVal = parseInt(options.minVal) }
		if((typeof options.minVal) != 'number') { options.minVal = 0 }
		
		if((typeof options.maxVal) == 'string') { options.maxVal = parseInt(options.maxVal) }
		if((typeof options.maxVal) != 'number') { options.maxVal = 100 }
		
		if((typeof options.values) != 'string') { options.values = 'percent' }
		switch (options.values){
			case 'percent':
			case 'numeric':
			case 'string': break;
			default: options.direction = 'percent'; break;
		}
		
		if((typeof options.steps) == 'string') { options.steps = parseInt(options.steps) }
		if((typeof options.steps) != 'number') { options.steps = 0 }
		if((options.steps % 1) != 0){
			options.steps = Math.round(options.steps)
		}
		
		if((typeof options.theme) != 'string') { options.theme = 'sliderInputDefault' }
		
		if((typeof options.displayTicks) != 'boolean') { options.displayTicks = true }
		if((typeof options.displayTooltip) != 'boolean') { options.displayTooltip = true }
		// end validate options
        
        // private vars, data holders
        var container, handle, theme, interval, currentTick, currentVal,
        	stops = [], stopsValues = [];

        var plugin = this;
        
        plugin.transitionDuration = '500ms'
        plugin.transitionTiming = 'cubic-bezier(.23,.47,.65,.96)'

        // plugin.settings.propertyName from inside the plugin or
        // myplugin.settings.propertyName from outside the plugin
        plugin.settings = {};

        // "constructor"
        var init = function() {
            plugin.settings = $.extend({}, defaults, options);

            plugin.el = el;
            plugin.callBack = callBack;
            
            plugin.container = $(el);
            
            plugin.theme = '.' + plugin.settings.theme;
            currentVal = plugin.settings.defaultVal;
                        
            setupValues();
            renderElements();
            setupKnob();
            
            if(plugin.settings.defaultVal != plugin.settings.minVal){
            	if(plugin.settings.steps > 0){
            		currentTick = stopsValues.indexOf(currentVal);
            		handle.setPositionX(stops[stopsValues.indexOf(currentVal)]);
        			handleMoved(stops[stopsValues.indexOf(currentVal)]);
            	} else {
            		cssOffset = parseInt($('.sliderKnob span').css('left'));
		        	cssTotal = parseInt($('.sliderTrackEmpty').css('width'));
		        	pos = (currentVal * (cssTotal - cssOffset)) / 100
		        	handle.setPositionX(pos);
		        	handleMoved(pos);
            	}
            }
        }

        // public methods
        // plugin.methodName(arg1, arg2, ... argn) from inside the plugin or
        // myplugin.publicMethod(arg1, arg2, ... argn) from outside the plugin
        
        // programatically move the handle
        plugin.moveHandleTo = function(pos) {
        	// if steps defined, snap to finite values
        	if(plugin.settings.steps > 0) {
        		pos = snapToTicks(pos)
        	}
        	        	
        	// compute current val for continuous sliders (no steps)
        	cssOffset = parseInt($('.sliderKnob span').css('left'));
        	cssTotal = parseInt($('.sliderTrackEmpty').css('width'));
        	currentVal = ( pos / (cssTotal - cssOffset) ) * 100;
        	
        	if((currentVal % 1) != 0){
        		currentVal = currentVal.toFixed(2);
        	}
        	
        	// move handle to computed pos
        	handle.setPositionX(pos);
        	
        	// move companion elements too (filled track and tooltip)
        	handleMoved(pos);
        	
        	// call-back if you want to do something while moving
			if(typeof callBack == 'function'){
		    	callBack(plugin.settings.steps > 0 ? (stopsValues[currentTick] + (plugin.settings.values == 'percent' ? '%' : '')) : (currentVal + (plugin.settings.values == 'percent' ? '%' : '')));
		    }
        }

        // private methods
        // methodName(arg1, arg2, ... argn)
        
        // load lesson title and moment names from json
		var renderElements = function() {
			// apply theme
			plugin.container.addClass(plugin.settings.theme)
			
			// place mandatory elements: container, track and knob
			plugin.container.html(
				'<div class="sliderInputContainer noTicks">' + 
					'<div class="sliderTrackEmpty"></div>' +
					'<div class="sliderTrackFilled"></div>' +
					'<div class="sliderKnob"><span></span></div>' +
				'</div>'
			)
						
			if(plugin.settings.direction == 'vertical'){
				$(el + ' .sliderInputContainer').css('-webkit-transform', 'rotate(-90deg)')
			}
			
			$(el + ' .sliderKnob span').css({
				'-webkit-transition-duration': plugin.transitionDuration,
				'-webkit-transition-timing-function': plugin.transitionTiming
			})
			
			// display ticks
			if(plugin.settings.displayTicks){
				plugin.container
					.find('.sliderInputContainer')
					.removeClass('noTicks')
					.addClass('withTicks')
					.end()
					.find('.sliderInputContainer')
					.prepend('<div class="sliderInputTicks"></div>')
					.end()
				setupSteps();
			} else {
				plugin.container
					.find('.sliderInputContainer')
					.prepend('<div class="sliderInputTicks hidden"></div>')
					.end()
				setupSteps();
			}
			
			if(plugin.settings.displayTooltip){
				plugin.container
					.find('.sliderKnob')
					.prepend('<div class="sliderInputTooltip"></div>')
					.end()
				updateTooltip();
				
				if(plugin.settings.direction == 'vertical'){
					$(el + ' .sliderInputTooltip').css('-webkit-transform', 'rotate(90deg)')
				}
			}
        }
        
        var setupSteps = function(){
        	var width, totalFloor, totalCeil
        		parse = 0;
        	
        	width = $('.sliderInputTicks').html('').width();
        	
        	if(plugin.settings.steps == 0){
        		// if no steps, display only first and last
        		interval = width - 2;
        		$('.sliderInputTicks')
        			.append('<span class="tick ' + plugin.settings.minVal + '"></span>')
        			.append('<span class="tick ' + plugin.settings.maxVal + '"></span>')
        	} else {
        		// if steps are given
        		interval = ((width - 1) / (plugin.settings.steps)) - 1;
        		
        		// check which method is the closest match, without getting over the width limit
        		totalFloor = ((Math.floor(interval) + 1) * (plugin.settings.steps + 1)) + 1;
        		totalCeil = ((Math.ceil(interval) + 1) * (plugin.settings.steps + 1)) + 1;
        		if(totalCeil > width) { totalCeil = -9999} // drop value if it's over the total width
        		
        		if((width - totalFloor) < (width - totalCeil)){
        			interval = Math.floor(interval)
        		} else {
        			interval = Math.ceil(interval)
        		}
        		
        		// add the tick marks
	        	while(parse < (plugin.settings.steps + 1)){
	        		$('.sliderInputTicks').append('<span class="tick val' + stopsValues[parse].toFixed(0) + '"></span>');
	        		stops.push(parse == 0 ? 0 : (stops[parse-1] + 1 + interval))
	        		parse++;
	        	}
        	}
        	        	
        	// apply spacing between marks
        	$('.sliderInputTicks .tick')
        		.css('margin-right', interval + 'px')
        		.last()
        		.css('margin-right', '0')
        }
        
        var setupValues = function(){
        	var valuesInterval;
        	
        	if(plugin.settings.steps > 0){
        		valuesInterval = (plugin.settings.maxVal - plugin.settings.minVal) / plugin.settings.steps;
        		
        		stopsValues.push(plugin.settings.minVal);
        		
        		for (parse = 1; parse < plugin.settings.steps; parse++) {
        			stopsValues.push(stopsValues[parse-1] + valuesInterval)
        		}
        		
        		stopsValues.push(plugin.settings.maxVal);
        	}
        }
        
        var setupKnob = function(){
        	var containerW, trackW, margin, knobW;
        	
        	containerW = $(el + ' .sliderInputContainer').width();
        	trackW = $(el + ' .sliderTrackEmpty').width();
        	margin = (containerW - trackW) / 2;
        	knobW = $(el + ' .sliderKnob span').width();
        	
        	handle = new ipadTouchDrag(el + ' .sliderKnob span', {
        		onlyX: true,
        		minX: margin - (knobW / 2),
        		maxX: trackW + margin - (knobW / 2)
        	}, function knobMoved(posx, posy, ev){        		
        		if (ev == 'touchMove'){
        			handleMoved(posx)
        		}
        		
        		if (ev == 'touchEnd') {
        			// compute current val for continuous sliders (no steps)
        			cssOffset = parseInt($('.sliderKnob span').css('left'));
		        	cssTotal = parseInt($('.sliderTrackEmpty').css('width'));
		        	currentVal = ( posx / (cssTotal - cssOffset) ) * 100;
		        	
		        	if((currentVal % 1) != 0){
		        		currentVal = currentVal.toFixed(2);
		        	}
        			        			
        			if (plugin.settings.steps > 0){
        				plugin.moveHandleTo(snapToTicks(posx))
        				handleMoved(snapToTicks(posx))
        			} else {
        				handleMoved(posx);
        			}
        		}
        		
        		// call-back if you want to do something while moving
				if(typeof callBack == 'function'){
			    	callBack(plugin.settings.steps > 0 ? (stopsValues[currentTick] + (plugin.settings.values == 'percent' ? '%' : '')) : (currentVal + (plugin.settings.values == 'percent' ? '%' : '')));
			    }
        	})
        }
        
        var snapToTicks = function(pos){
        	var delta, deltaNew, closest;
        	
        	delta = $('.sliderInputTicks').width();
        	
        	for (var parse = 0; parse < plugin.settings.steps + 3; parse++){
        		deltaNew = Math.abs(pos - stops[parse]);
        		if( deltaNew < delta ) {
					delta = deltaNew;
					closest = parse;
				}
        	}
        	
        	currentTick = closest;
        	
        	return stops[currentTick];
        }
        
        var handleMoved = function(pos){        	
        	$(el + ' .sliderTrackFilled').css('width', pos)
        	if(plugin.settings.displayTooltip){
        		$(el + ' .sliderInputTooltip').css('left', pos);
        		updateTooltip()
        	}
        }
        
        var updateTooltip = function(){
        	if(plugin.settings.displayTooltip){
        		if (plugin.settings.steps > 0){
        			$(el + ' .sliderInputTooltip').html(stopsValues[currentTick] + (plugin.settings.values == 'percent' ? '%' : ''));
        		} else {
        			$(el + ' .sliderInputTooltip').html(currentVal + (plugin.settings.values == 'percent' ? '%' : ''));
        		}
        	}
        }
        
        init();
    }
})(jQuery);