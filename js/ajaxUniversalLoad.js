/*
 * Developed by Marius Stuparu for Siveco Romania SA
 * Copyright (c) 2011 Siveco Romania SA http://www.siveco.ro
 * 
 * Version 1.0
 * 
 * Feel free to modify it to suit your needs, but do not delete this legal warning,
 * of if you minify automatically, please put a reference somewhere else.
 * 
 * Usage:
 * 
 */
;(function($) {
	
    jsonUniversalLoad = function(options, callBack) {

        // plugin's default options
        var defaults = {
            pathToJson: 'json/',
            fileName: 'data.json',
            objectDepth: 2 // maximum four levels supported for now; minimum is 1
        }
        
        // options validation
        if((typeof options.pathToJson != 'string') && (options.pathToJson != undefined)) {
        	console.error('ERROR! The path to your JSON file is of wrong type. It should be a string ending in /')
			return false;
		}
        if(options.pathToJson.chartAt(options.pathToJson.length - 1) != '/') { options.pathToJson += '/' }
        
        if((typeof options.fileName != 'string') && (options.pathToJson != undefined)) { 
        	console.error('ERROR! The JSON file name is of wrong type. It should be a string.');
        	return false;
        }
        
        if(options.fileName.indexOf('.') == -1){
        	try {
        		var tempTest;
        		
        		tempTest = $.get(pathToJson + fileName).error(function(){ throw 'Load error' })
        	}
        	catch(e){
        		if(e ==  'Load error'){
        			console.error('ERROR! Your file name is missing the "." dot and could not be loaded. Please check the options and the file');
        			return false;
        		}
        	}
        }
        
        if((typeof options.objectDepth == 'string')) { options.objectDepth = parseInt(options.objectDepth) }
        
        if((typeof options.objectDepth != 'number') && (options.objectDepth != undefined)){
        	console.warn('WARNING! The objectDepth option is of wrong type - should be an integer or string. Using the default value of 2');
        	options.objectDepth = 2;
        }
        
        if(typeof callBack != 'function'){
        	console.error('ERROR! No callback function provided. What do I do with the data?');
        	return false;
        }
        // end options validation
        
        var plugin = this;

        // plugin.settings.propertyName from inside the plugin or
        // myplugin.settings.propertyName from outside the plugin
        plugin.settings = {};

        // "constructor"
        var init = function() {
            plugin.settings = $.extend({}, defaults, options);

            loadFile();
            populateData();
            sendDataToCallback();
        }

        // public methods
        // plugin.methodName(arg1, arg2, ... argn) from inside the plugin or
        // myplugin.publicMethod(arg1, arg2, ... argn) from outside the plugin
        
        // load the html content for moment, push it into container
        plugin.foo = function() {
        	
        }

        // private methods
        // methodName(arg1, arg2, ... argn)
        
        // load lesson title and moment names from json
		var loadFile = function() {
			var jsonData = '';
			
            $.ajax({
				url: plugin.settings.pathToJson + plugin.settings.fileName,
				dataType: 'json',
				data: jsonData,
				async: false, // with async true, data is not written to variables
				success: function(jsonData){
					populateData(jsonData)
				},
				error: function(err){
					console.error('ERROR: There was an error loading your JSON file: ' + plugin.settings.pathToJson + plugin.settings.fileName + '\nerr.message')
				}
			});
        }
        
        // push data into variables
        var populateData = function(jsonData) {
        	$.each(jsonData, function(key1, val1){
        		// first and minimal tree level
        		if(plugin.settings.objectDepth > 0){
        			keyLevel1.push('' + key1)
        			// second level
        			if(plugin.settings.objectDepth > 1){
        				$.each(val1, function(key2, val2){
        					keyLevel2.push('' + key2)
        					// third level
        					if(plugin.settings.objectDepth > 2){
        						$.each(val2, function(key3, val3){
        							keyLevel3.push('' + key3)
        							//forth level
        							if(plugin.settings.objectDepth > 3){
        								$.each(val3, function(key4, val4){
        									keyLevel4.push('' + key4)
        									valLevel4.push('' + val4)
        									// if you really need more than 4 level depth, consider braking the information in multiple files
        									// or extend this with further levels, but I don't recommend it as it will impact on speed
        								})
        							} else {
        								valLevel3.push('' + val3)
        							}
        						})
        					} else {
        						valLevel2.push('' + val2)
        					}
        				})
        			} else {
        				valLevel1.push('' + val1)
        			}
        		}
        	})
        }

        init();
    }
})(jQuery);