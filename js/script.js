var errorThrown = false,
	options = {},
	currentPage = 1;

/* --------------------------------------------
 *         Load and populate content
   --------------------------------------------*/
var loadSettings = function(){
	$.ajax({
		url: 'lesson/js/settings.js',
		dataType: 'script',
		async: false,
		success: function(){
			options = $.extend({}, settings)
		},
		error: function(x, e){
			errorThrown = true;
			if(x.status==0){
				alert('You are offline!\n Please check your network.');
			}else if(x.status==404){
				alert('An important file (settings.js) is missing.\nPlease contact support.');
			}else if(x.status==500){
				alert('Internel Server Error.');
			}else if(e=='timeout'){
				alert('Request Time out.');
			}else {
				lert('Unknow Error.\n'+x.responseText);
			}
			return false;
		}
	})
}

var populateGenericItems = function(){
	try {
		document.title = options.lessonTitle;
		
		$('#lesson_title')
			.find('h1')
			.addClass(options.textDirection)
			.html(options.lessonTitle)
			.end()
			.find('h2')
			.addClass(options.textDirection)
			.html(options.lessonSubtitle)
			.end()
		$('footer')
			.addClass(options.textDirection)
			.html(options.lessonFooter)
		$('#theory')
			.find('.controls_help')
			.html('<ul>' + controls_help.join('') + '</ul>')
			.end()
			.find('.theory_details')
			.html(theory_details.join(''))
			.end()
		
		setupNavigation()
		
	} catch (err) {
		if (err.message == "Can't find variable: options"){
			alert('There was an error loading a core file: malformed data in settings.js\nPlease contact support.')
		}
	}	
}

var loadChapter = function(pageName){
	currentPage = pageName.slice(4)
	
	// load the lesson template
	$('aside #theory')
		.addClass(options.textDirection)
		.find('.instructions')
		.html(page[currentPage].instructions)
	
	$.ajax({
		url: 'lesson/' + pageName + '.html',
		dataType: 'html',
		async: false,
		success: function(htmlTemplate){
			$('#content_top').html('<iframe id="' + pageName + '" src="lesson/' + pageName + '.html"></iframe>')
		},
		error: function(x, e){
			errorThrown = true;
			if(x.status==0){
				alert('You are offline!\n Please check your network.');
			}else if(x.status==404){
				alert('An important file (lesson/' + pageName + '.html) is missing.\nPlease contact support.');
			}else if(x.status==500){
				alert('Internel Server Error.');
			}else if(e=='timeout'){
				alert('Request Time out.');
			}else {
				lert('Unknow Error.\n'+x.responseText);
			}
			return false;
		}
	})
	
	setupNavigationActions();
	
	populateTheory();
}

var populateTheory = function(){
	
}

var setupNavigation = function(){
	var nav_items = [];
	
	if(options.numPages > 1){
		// build the list of bulltets
		if (options.generalDirection == 'rtl'){
			for(parse = options.numPages; parse > 0; parse--) {
				nav_items.push('<li class="bullet pag_' + parse + '" rel="pag_' + parse + '"></li>')
			}
		} else {
			for(parse = 1; parse < options.numPages + 1; parse++) {
				nav_items.push('<li class="bullet pag_' + parse + '"></li>')
			}
		}

		$('#page_nav')
			.html('<ul><li class="arrow arrow_left"></li>' + nav_items.join('') + '<li class="arrow arrow_right"></ul>')
			.fadeIn(1000)
	}
}

var setupNavigationActions = function(){
	// bind actions for the bullets
	$('#page_nav .bullet').each(function(){
		$(this)
			.removeClass('disabled')
			.bind('click', function(){
				var page_name = $(this).attr('rel')
				loadChapter(page_name)
			})
	})
	
	// set the left/right arrows based on the writing direction
	if(options.generalDirection == 'rtl'){
		$('#page_nav .arrow_left')
			.unbind()
			.removeClass('disabled')
			.bind('click', function(){ loadNextPage() })
		
		$('#page_nav .arrow_right')
			.unbind()
			.removeClass('disabled')
			.bind('click', function(){ loadPrevPage() })
	} else {
		$('#page_nav .arrow_left')
			.unbind()
			.removeClass('disabled')
			.bind('click', function(){ loadPrevPage() })
		
		$('#page_nav .arrow_right')
			.unbind()
			.removeClass('disabled')
			.bind('click', function(){ loadNextPage() })
	}
	
	// disable current bullet
	$('#page_nav .pag_' + currentPage).addClass('disabled').unbind()
	
	// disable terminal arrows
	if (currentPage == 1){
		if (options.generalDirection == 'rtl'){
			$('#page_nav .arrow_right').addClass('disabled').unbind()
		} else {
			$('#page_nav .arrow_left').addClass('disabled').unbind()
		}
	}
	
	if (currentPage == options.numPages){
		if (options.generalDirection == 'rtl'){
			$('#page_nav .arrow_left').addClass('disabled').unbind()
		} else {
			$('#page_nav .arrow_right').addClass('disabled').unbind()
		}
	}
}

var loadNextPage = function() {
	if(currentPage < options.numPages){
		$('#content_top').html('loading...')
		//currentPage++
		loadChapter('pag_' + (++currentPage))
	}
}

var loadPrevPage = function() {
	if(currentPage > 0){
		$('#content_top').html('loading...')
		//currentPage--
		loadChapter('pag_' + (--currentPage))
	}
}

/* --------------------------------------------
 *              Auxiliary helpers
   --------------------------------------------*/
var attachHelperFunctions = function(){
	// see version number on logo double click
	$('#lesson_logo').bind('dblclick', function(){
		$('footer').append('<span class="appVersion">(app version ' + options.appVersion + ')</span>')
		window.setTimeout(function(){
			$('footer .appVersion').remove()
		}, 5000, true);  
	})
	
	// toggle action for the help button
	$('#theory .left_button').bind('click', function(){
		if($(this).hasClass('question')){
			$(this)
				.removeClass('question')
				.addClass('left')
			$('#theory')
				.find('.center_icon')
				.removeClass('theory')
				.addClass('help')
				.end()
				.find('.right_button')
				.hide()
				.end()
				.find('.instructions')
				.hide()
				.end()
				.find('.controls_help')
				.show(500)
				.end()
			return false;
		}
		
		if($(this).hasClass('left')){
			$(this)
				.removeClass('left')
				.addClass('question')
			$('#theory')
				.find('.center_icon')
				.removeClass('help')
				.addClass('theory')
				.end()
				.find('.right_button')
				.show()
				.end()
				.find('.controls_help')
				.hide()
				.end()
				.find('.instructions')
				.show(500)
				.end()
			return false;
		}
	})
	
	// toggle action for the details button
	$('#theory .right_button').bind('click', function(){
		if($(this).hasClass('theory')){
			$(this)
				.removeClass('theory')
				.addClass('right')
			$('#theory')
				.find('.center_icon')
				.removeClass('theory')
				.addClass('details')
				.end()
				.find('.left_button')
				.hide()
				.end()
				.find('.instructions')
				.hide()
				.end()
				.find('.theory_details')
				.show(500)
				.end()
			return false;
		}
		
		if($(this).hasClass('right')){
			$(this)
				.removeClass('right')
				.addClass('theory')
			$('#theory')
				.find('.center_icon')
				.removeClass('details')
				.addClass('theory')
				.end()
				.find('.left_button')
				.show()
				.end()
				.find('.theory_details')
				.hide()
				.end()
				.find('.instructions')
				.show(500)
				.end()
			return false;
		}
	})
}

/* --------------------------------------------
 *              Init function
   --------------------------------------------*/
$(function(){
	// prevent bounce scroll
	$(document).bind('touchmove', function (e) {
		e.preventDefault();
	});
	
	loadSettings()
	
	if (!errorThrown){
		populateGenericItems();
		attachHelperFunctions();
		loadChapter('pag_1');
	} else {
		$('html').html('<h1>There was an error loading the lesson</h1><h2>Please contact support</h2>')
	}

})
