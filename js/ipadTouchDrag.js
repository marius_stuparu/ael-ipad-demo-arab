/*
 * Developed by Marius Stuparu for Siveco Romania SA
 * Copyright (c) 2011 Siveco Romania SA http://www.siveco.ro
 * 
 * Version 1.2
 * 
 * Inspiration for prototype from some work from Matteo Spinelli, http://cubiq.org/
 * Inspiration for touchmove handling on iPad from Peter-Paul Koch, http://www.quirksmode.org/
 * 
 * Feel free to modify it to suit your needs, but do not delete this legal warning,
 * of if you minify automatically, please put a reference somewhere else.
 * 
 * Usage:
 * Load the plug-in in the HTML page with something like <script src="js/ipadTouchDrag.js"></script>
 * Movable object must be absolute or relative placed 
 * Call it inside your main script file like this:
 * 
   var whatever = new ipadTouchDrag('selector', { // selector can be anything from #id, .class, tag
		// put your options overrides here
		onlyY: true, //see below in default for all options
		minY: 0,
		maxY: 250
	}, function callBackFn (posX, posY, ev){
		// do anything with the X and Y coordinates and the event type
		// check the prototype functions for further details
		// posX and posY are given unit-less, add + 'px' for css manipulation
		// ev will be a string, 'touchStart', 'touchMove', 'touchEnd'
	});
 */

function ipadTouchDrag (el, options, callBack) {
	var defaults = {
		onlyX: false, // move only on X axis
		onlyY: false, // move only on Y axis
		minX: undefined, // --------------------------------------------------
		maxX: undefined, // you can constrain movement to a smaller rectangle,
		minY: undefined, // default is unconstrained
		maxY: undefined  // --------------------------------------------------
	}
	
	var originalX, originalY, posX, posY, dX, dY;
	
	// validate options
	if((typeof options.onlyX) != 'boolean') { options.onlyX = false }
	if((typeof options.onlyY) != 'boolean') { options.onlyY = false }
	if((typeof options.minX) == 'string') { options.minX = parseInt(options.minX) }
	if((typeof options.minX) != 'number') { options.minX = undefined }
	if((typeof options.maxX) == 'string') { options.maxX = parseInt(options.maxX) }
	if((typeof options.maxX) != 'number') { options.maxX = undefined }
	if((typeof options.minY) == 'string') { options.minY = parseInt(options.minY) }
	if((typeof options.minY) != 'number') { options.minY = undefined }
	if((typeof options.maxY) == 'string') { options.maxY = parseInt(options.maxY) }
	if((typeof options.maxY) != 'number') { options.maxY = undefined }
	// end validate options
	
	this.settings = {};

	if (jQuery) {  
	    this.settings = $.extend({}, defaults, options);
	} else {
	    this.settings = defaults;
		for (var p in options) {
			if(options.hasOwnProperty(p)){
				if(this.settings[p] === false || this.settings[p] === undefined)
					this.settings[p] = options[p]
				else {
					n = 2;
					while(this.settings[p+n] != undefined)++n
						this.settings[p+n] = options[p]
				}
			}
		}
	}

	this.handle = document.querySelector(el);
	this.callBack = callBack;
	
	this.originalX = this.handle.offsetLeft; //
	this.originalY = this.handle.offsetTop; //
	
	this.posX = this.originalX;
	this.posY = this.originalY;
	
	// add listeners for touch events
	this.handle.addEventListener('touchstart', this);
	
	// so we can call it from outsite, when programatically moving the object
	this.setPosition = function(posx, posy){
		if (!this.settings.onlyY) {
			// validate input
			if(typeof posx == 'number'){
				this.posX = posx;
			}
			
			// check for out of range
			if(typeof(this.settings.minX) != 'undefined'){
				if (posx < this.settings.minX){
					this.posX = this.settings.minX;
				}
			}
			
			if(typeof(this.settings.maxX) != 'undefined'){
				if(this.posX > this.settings.maxX){
					this.posX = this.settings.maxX;
				}
			}
		}
		
		if (!this.settings.onlyX) {
			// validate input
			if(typeof posy == 'number'){
				this.posY = posy;
			}
			
			// check for out of range
			if(typeof(this.settings.minY) != 'undefined'){
				if(this.posY < this.settings.minY){
					this.posY = this.settings.minY;
				}
			}
			
			if(typeof(this.settings.maxY) != 'undefined'){
				if(this.posY > this.settings.maxY){
					this.posY = this.settings.maxY;
				}
			}
		}
		
		// apply new position
		//this.handle.style.webkitTransform = 'translate3d(' + this.posX + 'px,' + this.posY + 'px,0)';
		this.handle.style.left = this.posX + 'px';
		this.handle.style.top = this.posY + 'px';
		
		if(typeof this.callBack == 'function'){
	      this.callBack(this.posX, this.posY, 'manualMove');
	    }
	}
	
	this.setPositionX = function(posx, forceOutside){
		// validate input
		if(typeof posx == 'number'){
			this.posX = posx;
		}
		
		// check for out of range
		if(typeof(this.settings.minX) != 'undefined'){
			// forceOutside is a hack for moving the object out of boundries (i.e. hiding outside the screen)
			if (this.posx < this.settings.minX && !forceOutside){
				this.posX = this.settings.minX;
			}
		}
		
		if(typeof(this.settings.maxX) != 'undefined'){
			if(this.posX > this.settings.maxX && !forceOutside){
				this.posX = this.settings.maxX;
			}
		}
		
		// apply new position
		this.handle.style.webkitTransform = 'translate3d(' + this.posX + 'px,0,0)';
		
		if(typeof this.callBack == 'function'){
	      this.callBack(this.posX, 'manualMoveX');
	    }
	}
	
	this.setPositionY = function(posy, forceOutside){
		// validate input
		if(typeof posy == 'number'){
			this.posY = posy;
		}
		
		// check for out of range
		if(typeof(this.settings.minY) != 'undefined'){
			// forceOutside is a hack for moving the object out of boundries (i.e. hiding outside the screen)
			if(this.posY < this.settings.minY && !forceOutside){
				this.posY = this.settings.minY;
			}
		}
		
		if(typeof(this.settings.maxY) != 'undefined'){
			if(this.posY > this.settings.maxY && !forceOutside){
				this.posY = this.settings.maxY;
			}
		}
		
		// apply new position
		this.handle.style.webkitTransform = 'translate3d(0,' + this.posY + 'px,0)';
		
		if(typeof this.callBack == 'function'){
	      this.callBack(this.posY, 'manualMoveY');
	    }
	}
}

// prototype for object instances
ipadTouchDrag.prototype = {
	// event handler function, dispatches calls according to the event
	handleEvent: function(e) {
		switch (e.type) {
			case 'touchstart': this.touchStart(e); break;
			case 'touchmove': this.touchMove(e); break;
			case 'touchend': this.touchEnd(e); break;
		}		
	},
	
	// touchstart event, it only calls the call-back function
	touchStart: function(e) {
    	// prevent normal scroll on iOS
    	e.preventDefault();
		e.stopPropagation();

		var targetEvent = e.touches.item(0),
    		touchedX = targetEvent.clientX,
    		touchedY = targetEvent.clientY;
    		
    	this.dX = touchedX - this.posX;
    	this.dY = touchedY - this.posY;
    	
		// call-back function if you want to to something before movement starts
		if(typeof this.callBack == 'function'){
	      this.callBack(this.posX, this.posY, 'touchStart');
	    }
	    
	    this.handle.addEventListener('touchmove', this);
		this.handle.addEventListener('touchend', this);
	    
	    return false;
	},
	
	touchMove: function(e) {
		e.preventDefault(); // prevent normal scroll so it can actually move
		e.stopPropagation();
		
    	var targetEvent = e.touches.item(0),
    		touchedX = targetEvent.clientX,
    		touchedY = targetEvent.clientY;
		
		// here starts actual movement computing
		// TO DO - add web workers
				
		// if there's no restriction for X axis, compute posX
		if (!this.settings.onlyY) {
			// compensate for finger position
			this.posX = touchedX - this.dX;

			// if there are region constraints, apply them

			if(typeof(this.settings.minX) != 'undefined'){
				if(this.posX < this.settings.minX){
					this.posX = this.settings.minX;
				}
			}
			
			if(typeof(this.settings.maxX) != 'undefined'){
				if(this.posX > this.settings.maxX){
					this.posX = this.settings.maxX;
				}
			}
		}
		
		// if there's no restriction for Y axis, compute posY
		if (!this.settings.onlyX) {
			// compensate for finger position
			this.posY = touchedY - this.dY;
			
			// if there are region constraints, apply them
			if(typeof(this.settings.minY) != 'undefined'){
				if(this.posY < this.settings.minY){
					this.posY = this.settings.minY;
				}
			}
			
			if(typeof(this.settings.maxY) != 'undefined'){
				if(this.posY > this.settings.maxY){
					this.posY = this.settings.maxY;
				}
			}
		}

		this.setPosition();
		
		// call-back if you want to do something while moving
		if(typeof this.callBack == 'function'){
	      this.callBack(this.posX, this.posY, 'touchMove');
	    }
	    
	    return false;
	},
	
	touchEnd: function(e) {
        // call-back if you want to do something at the end of movement
		if(typeof this.callBack == 'function'){
	      this.callBack(this.posX, this.posY, 'touchEnd');
	    }
	    
	    this.handle.removeEventListener('touchmove', this);
		this.handle.removeEventListener('touchend', this);
	    
	    return false;
	}
}