var settings = {
	textDirection: 'arab_rtl',
	generalDirection: 'rtl',
	numPages: 1,
	appVersion: '0.1b',
	lessonTitle: 'خصائص و تحضير وتفاعلات المركبات العضوية (أحماض كربوكسيلية)',
	lessonSubtitle: 'تفاعل  الأحماض الكربوكسيلية مع الكربونات',
	lessonFooter: 'الكيمياء'
}

var controls_help = [
	'<li><span class="control validate"></span>التحقق من صحة / التحقق</li>',
	'<li><span class="control back"></span>ظهر</li>',
	'<li><span class="control forward"></span>إلى الأمام</li>',
	'<li><span class="control play"></span>تبدأ الرسوم المتحركة</li>',
	'<li><span class="control pause"></span>وقفة</li>',
	'<li><span class="control stop"></span>توقف</li>',
	'<li><span class="control feedback"></span>الاقتراحات</li>',
	'<li><span class="control help"></span>مساعدة</li>',
	'<li><span class="control reset"></span>إعادة تعيين</li>',
	'<li><span class="control_big test"></span>التحقق من المعرفة</li>',
	'<li><span class="control_big lesson"></span>عودة إلى الدرس</li>',
	'<li><span class="control_big navigate"></span>التنقل بين الإطارات</li>',
	'<li><span class="control_big resources"></span>الموارد الأخرى</li>'
]

var theory_details = [
	''
]

var page = []
page[1] = {
	instructions: '<p>إنَ الأحماض أحادية الكربوكسيل الأليفاتية  المشبعة هي أحماض ضعيفة. حمض الميثانويك, هو أقوى حمض كربوكسيلي من هذا النوع, حيث إن ثابت تأين الحمض الخاص به KA =1.8 &bull; 10<sup>-4</sup> .</p>'+
	'<p>الخواص الحمضية الخاصة بالأحماض الكربوكسيلية ترجع إلى حركة الإلكترونات بين مجموعة الهيدروكسيل ومجموعة  الكربونيل, '+
	'مما يؤدي إلى زيادة تأين رابطة  (O-H) وانطلاق البروتون(H +) كاتيون</p>'+
	'<p>وجود مجموعة الكربونيل أدى إلى جذب الكثافة الإلكترونية من منطقة الهيدروكسيل, مما أدى إلى '+
	'وانطلاق الهيدروجين كبروتون في المحاليل المائية.</p>'+
	'<p>أحماض الكربوكسيل هي أحماض ذات طبيعة حمضية ضعيفة، و يحدث لها تأين جزئي في المحاليل المائية. أوثابت التأين للأحماض عند درجة حرارة 25 ° C.</p>'+
	'<p>K<sub>a</sub> = 10<sup>-4</sup> TO 10<sup>-5</sup></p>'+
	'<p>الأحماض الكربوكسيلة  لها نفس</p>'+
	'<p>تفاعلات الأحماض الأخرى كما في التفاعلات التالية :</p><ul>'+
	'<li>مع المعادن النشطة والتي ينتج عنها الهيدروجين</li>'+
	'<li>مع القلويات</li>'+
	'<li>تغيير لون الكواشف الكيمائية  كما هو الحال في تباع الشمس.</li>'+
	'<li>تتفاعل مع  أملاح الأحماض الضعيفة، كما هو الحال مع أملاح الكربونات (K<sub>H<sub>2</sub>CO<sub>3</sub></sub>&nbsp;=&nbsp;4,3&nbsp;&bull;&nbsp;10<sup>-7</sup>)</li></ul>'+
	'<p>حمض الكربونيك يتحول في الماء إلى ثاني أكسيد الكربون.</p>',
	fix_tube: 'قم بتثبيت الأنبوبة في المكان المناسب على الحامل وشاهد عرض الوسائط المتعددة وسجل ملاحظاتك.',
	click_funnel: 'اضغط على القمع لإضافة القليل من الخل إلى القارورة.',
	choose_answer: 'اختر الإجابة الصحيحة ثم اضغط على ]زر[ تأكد.',
	click_next: 'اضغط التالي.',
	ch3c00h: 'الخل (CH3COOH)',
	caco3: 'كربونات الكالسيوم (CaCO3)',
	caoh2: 'هيدروكسيد الكالسيوم(Ca(OH)2)'
}