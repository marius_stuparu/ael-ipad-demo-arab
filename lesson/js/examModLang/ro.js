var lng = {
	reset: 'ȘTERGE',
	answer: 'RĂSPUNDE',
	prev: 'ANTERIOR',
	next: 'URMĂTOR',
	pageSwitchConfirm: 'Ai întrebări la care ai răspuns, dar nu le-ai testat.\nSigur vrei să schimbi pagina?'
}
