var lng = {
	direction: 'rtl',
	reset: 'إعادة',
	answer: 'أجاب',
	prev: 'سابق',
	next: 'التالي',
	pageSwitchConfirm: 'هل لديك أسئلة أجبت، ولكن لم يتم اختباره.\nهل أنت متأكد أنك تريد تغيير الصفحة؟'
}
