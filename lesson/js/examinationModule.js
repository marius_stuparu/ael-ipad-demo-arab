/*
 * Developed by Marius Stuparu for Siveco Romania SA
 * Copyright (c) 2011 Siveco Romania SA http://www.siveco.ro
 * 
 * Version 1.0
 * 
 * You can style its appearance using examinationModule.css
 * 
 * Feel free to modify it to suit your needs, but do not delete this legal warning,
 * of if you minify automatically, please put a reference somewhere else.
 * 
 * Usage:
 * 
 * 1. Load the script in your page, AFTER jQuery, but before your main script, like this:
 * 		<script src="js/examinationModule.js"></script>
 * 
 * (make sure the language file is in its folder, default /js/examModLang/)
 * 
 * 2. Load the CSS file, like this (you can add your own theme here, or modify the default):
 * 		<link rel="stylesheet" href="css/examinationModule.css">
 * 
 * 3. Write the questions and answers in your HTML:
 * 		<div class="exam"> <!-- this is the container ->
		<div class="questions"> <!-- each child here becomes one question per page -->
			<h3>Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo.</h3>
			<h4><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas.</h4>
		</div>
		<div class="info_page"> <!-- if you want to display a help overlay, put content here -->
			<p>Your help content</p>
		</div>
		<div class="page_1"> <!-- for multi-page exam, otherwise you can leave this without a class -->
			<ul class="single"> <!-- only one ul per page, class must be "single" or "multiple"
				<li>Answer 1</li>
				<li>Answer 2</li>
				<li>Answer 3</li>
			</ul>
		</div>
		<div class="page_2">
			<ul class="multiple">
				<li>Answer 1</li>
				<li>Answer 2</li>
				<li>Answer 3</li>
			</ul>
		</div>
 * 4. Call the plugin with your options:
 * var examModule = new examinationModule('.exam', {
		questionIdentifier: '.questions',
		numPages: 2,
		pagesIdentifiers: ['.page_1', '.page_2'],
		correctAnswers: [[2], [1,2]] // these are the correct answers, one sub-array per page
		// more options below
	})
 * 
 * You can translate the buttons and messages using the translation file provided
 */
;(function($) {
	
    examinationModule = function(el, options, callBack) {

        // plugin's default options
        var defaults = {
            theme: 'defaultExaminationModule', 			// default visual theme
            questionIdentifier: '.question', 			// selector for element containing the question
            numPages: 1, 								// if there are multiple pages, increase this number
            pagesIdentifiers: ['.page_1', '.page_2'], 	// if multiple pages, their selectors
            infoButtonPresent: true, 					// does the module have an Info button?
            infoContainer: '.info_page', 				// if there is an Info button, this is the selector for its content
            correctAnswers: [[1], [1,2,3]], 			// arrays containing the correct answers for each page
            lang: 'ro'
        }

        var plugin = this;

        plugin.settings = {};

        // "constructor"
        var init = function() {
            plugin.settings = $.extend({}, defaults, options);

            plugin.el = el;
            plugin.callBack = callBack;
            plugin.container = $(el);
            
            plugin.theme = '.' + plugin.settings.theme;
            
            plugin.groups = new Array();
            plugin.values = new Array();
            for(i=0; i<plugin.settings.numPages; i++){
            	plugin.values[i] = new Array();
            }
            
            loadLanguage();
            
            fillElements();
        }
        
        var currentPage = 0, showingSolveBtns = false, answeredOK = false, 
        	singleAnswer = undefined, multipleAnswers = [];

        // public methods
        
        // load translation variables
        var loadLanguage = function(){
        	$.ajax({
        		url: 'js/examModLang/' + plugin.settings.lang + '.js',
        		dataType: 'script',
        		async: false,
        		error: function(){
        			throw new Error('ERROR! The language file is missing, cannot continue without it! (js/examModLang/' + plugin.settings.lang + '.js)')
        			return false;
        		}
        	})
        }
        
        // load lesson title and moment names from json
		var fillElements = function() {
			$(el)
				.addClass(plugin.settings.theme)
				.append('<div class="buttons"></div>')
				.find(plugin.settings.questionIdentifier)
				.addClass('question unanswered')
				.end()
				.find(plugin.settings.infoContainer)
				.hide()
				.end()
			
			// if multiple pages, hide all but first
			if(plugin.settings.numPages > 1){
				var parser = 0;
				
				while(parser < plugin.settings.numPages) {
					$(el + ' ' + plugin.settings.questionIdentifier + ' > *:eq(' + parser + ')')
						.addClass('question_' + (parser+1))
						.prepend('<span class="icon question_icon"></span>')
					$(el + ' ' + plugin.settings.pagesIdentifiers[parser]).addClass('page page_' + (parser+1))
					if (parser > 0){
						$(el + ' ' + plugin.settings.pagesIdentifiers[parser]).hide()
						$(el + ' ' + plugin.settings.questionIdentifier + ' .question_' + (parser+1)).hide()
					}
					parser++
				}
				
				displayPageNav();
			} else {
				$(el + ' > div').addClass('page')
			}
			
			// place buttons
			$(el + ' .buttons').append('<div class="solve_btns"><a name="reset" class="button reset disabled">' + lng.reset + '</a><a name="answer" class="button answer disabled">' + lng.answer + '</a></div>')
			
			// if there's an info box, display button
			if(plugin.settings.infoButtonPresent){
				var parentWidth = $(el).width()
				
				$(el + ' .page').css('margin-top', '10px')
				
				$(el + ' ' + plugin.settings.infoContainer)
					.addClass('info_container')
					.after('<div class="info_button"><span>i</span></div>')
					
				$(el + ' .info_button').bind('touchstart click', function(){
					displayHelpPage()
				})
			}
			
			formatLists();
        }
        
        // display previous / next buttons
        var displayPageNav = function() {
        	$(el + ' .buttons').prepend('<div class="page_nav"><a name="prev" class="button prev">' + lng.prev + '</a><a name="next" class="button next">' + lng.next + '</a></div>');
        	
        	enableButtons();
        }
        
        // replace li content with inputs and attach click events
        var formatLists = function(){
        	var ulItems = $(el + ' .page ul');
        	
        	ulItems.each(function(parseul){
        		var liItems = $(this).children('li'),
        			inputType = $(this).attr('class');
        			
        		if(inputType != 'single' && inputType != 'multiple'){
        			throw new Error('ERROR! The <ul> lists of answers must have a single class attribute named "single" or "multiple". This was the selector causing the problem: ' + $(this).parents().attr('class') + ' ul.' + inputType)
        		} else {
        			plugin.groups.push('grp' + parseul);
        		
	        		liItems.each(function(parseli){
	        			var temp = $(this).html()
	        			
	        			// replace lists with inputs
	        			$(this).html('<label for="grp' + parseul + 'val' + parseli + '"><input type="' + (inputType == 'single' ? 'radio' : 'checkbox') + '" value="' + parseli + '" name="' + (inputType == 'single' ? ('grp' + parseul) : ('val' + parseli)) + '" id="grp' + parseul + 'val' + parseli + '">' + temp + '</label>')
	        			
	        			// push ids into database
	        			plugin.values[parseul].push('grp' + parseul + 'val' + parseli)
	        			
	        			// bind action for change event
	        			var questionType = $(this).parents('ul').attr('class')
	        			switch(questionType){
	        				case 'single': {
	        					$(this).bind('click touchstart', function(){
	        						singleAnswer = $(this).index() + 1;
	        						enableSolveButtons();
	        					})
	        					break;
	        				}
	        				case 'multiple': {
	        					$(this).bind('mousedown touchstart', function(){
	        						var index = multipleAnswers.indexOf($(this).index() + 1)
	        						if (index == -1){
	        							multipleAnswers.push($(this).index() + 1);
	        							enableSolveButtons();
	        						} else {
	        							multipleAnswers.splice(index,1);
	        						}
	        					})
	        					
	        					break;
	        				}
	        			} // end switch
	        		}) // end each
        		} // end else
        	}) // end each
        }
        
        // enable / disable page navigation buttons based on current position
        var enableButtons = function(){
        	// no need for nav if only one page
        	if (plugin.settings.numPages == 1) { $(el + ' .page_nav').hide() }
        	
        	// disable previous and next on first and last page
        	if(plugin.settings.numPages > 1){
        		if (currentPage == 0) {
	        		$(el + ' .buttons')
	        			.find('.prev')
	        			.addClass('disabled')
	        			.unbind()
	        			.end()
	        			.find('.next')
	        			.removeClass('disabled')
	        			.bind('touchstart click', function(){
	        				nextPage()
	        			})
	        			.end()
	        	}
	        	
	        	if (currentPage == (plugin.settings.numPages - 1)) {
	        		$(el + ' .buttons')
	        			.find('.next')
	        			.addClass('disabled')
	        			.unbind()
	        			.end()
	        			.find('.prev')
	        			.removeClass('disabled')
	        			.bind('touchstart click', function(){
	        				prevPage()
	        			})
	        			.end()
	        	}
	        	
	        	if ((currentPage > 0) && (currentPage < plugin.settings.numPages - 1)) {
	        		$(el + ' .buttons')
	        			.find('.prev')
	        			.removeClass('disabled')
	        			.bind('touchstart click', function(){
	        				prevPage()
	        			})
	        			.end()
	        			.find('.next')
	        			.removeClass('disabled')
	        			.bind('touchstart click', function(){
	        				nextPage()
	        			})
	        			.end()
	        	}
        	}
        }
        
        // hide current page, display the previous one
        var prevPage = function(){
        	var doNotSwitch = false;
        	
        	if(showingSolveBtns && !answeredOK){
        		if(!confirm(lng.pageSwitchConfirm)){
        			doNotSwitch = true
        		}
        	}
        	
        	if (!doNotSwitch){
        		resetPage();
        		
        		$(el + ' ' + plugin.settings.questionIdentifier + ' .question_' + (currentPage + 1)).hide();
        		$(el + ' ' + plugin.settings.questionIdentifier + ' .question_' + currentPage).show();
        		
        		$(el)
	        		.find(plugin.settings.pagesIdentifiers[currentPage])
	        		.slideUp()
	        		.hide()
	        		.end()
	        		.find('.question_' + (currentPage + 1))
	        		.hide()
	        		.end()
	        		.find(plugin.settings.pagesIdentifiers[--currentPage])
	        		.slideDown()
	        		.show()
	        		.end()

	        	enableButtons()
	        	disableSolveButtons()
        	}
        }
        
        // hide current page, display the next one
        var nextPage = function(){
        	var doNotSwitch = false;
        	
        	if(showingSolveBtns && !answeredOK){
        		if(!confirm(lng.pageSwitchConfirm)){
        			doNotSwitch = true
        		}
        	}
        	
        	if (!doNotSwitch){
        		resetPage();

        		$(el + ' ' + plugin.settings.questionIdentifier + ' .question_' + (currentPage + 1)).hide();
        		$(el + ' ' + plugin.settings.questionIdentifier + ' .question_' + (currentPage + 2)).show();

	        	$(el)
	        		.find(plugin.settings.pagesIdentifiers[currentPage])
	        		.slideUp()
	        		.hide()
	        		.end()
	        		.find(plugin.settings.pagesIdentifiers[++currentPage])
	        		.slideDown()
	        		.show()
	        	enableButtons();
	        	disableSolveButtons();
	        }
        }
        
        // enable reset / answer buttons
        var enableSolveButtons = function(){
        	$(el + ' .buttons .solve_btns')
        		.find('.reset')
        		.removeClass('disabled')
        		.bind('touchstart click', function(){
        			resetPage()
        		})
        		.end()
        		.find('.answer')
        		.removeClass('disabled')
        		.bind('touchstart click', function(){
        			checkAnswer()
        		})
        		.end()
        	
        	showingSolveBtns = true;
        }
        
        // disable reset / answer buttons
        var disableSolveButtons = function() {
        	$(el + ' .buttons .solve_btns')
        		.children('a')
        		.addClass('disabled')
        		.unbind()
        	
        	showingSolveBtns = false;
        }
        
        // clear all checked inputs and right/wrong classes
        var resetPage = function(){
        	$(el + ' ' + plugin.settings.pagesIdentifiers[currentPage] + ' input').removeAttr('checked')
        	
        	$(el + ' ' + plugin.settings.questionIdentifier + ' .question_' + (currentPage+1))
        		.removeClass('wrong correct')
        		.find('.icon')
        		.removeClass('wrong_icon correct_icon')
        		.addClass('question_icon')
        		.end()
        		
        	singleAnswer = undefined;
        	multipleAnswers = [];
        	answeredOK = false;
        	disableSolveButtons()
        }
        
        // compare user input with correct answers
        var checkAnswer = function(){
        	var answerType = $(el + ' ' + plugin.settings.pagesIdentifiers[currentPage] + ' ul').attr('class');
        	
        	switch(answerType) {
        		case 'single': {
        			if(singleAnswer == plugin.settings.correctAnswers[currentPage]){
        				markRightWrongAnswers('single', 'correct')
        			} else {
        				markRightWrongAnswers('single', 'wrong')
        			}
        		} break;
        		case 'multiple': {
        			plugin.settings.correctAnswers[currentPage].sort();
        			multipleAnswers.sort();
        			        			
        			var countCorrect = 0;
        			
        			if((plugin.settings.correctAnswers[currentPage].length) == multipleAnswers.length){
        				for(var i=0; i<multipleAnswers.length; i++){
        					if(multipleAnswers[i] == plugin.settings.correctAnswers[currentPage][i]){
        						countCorrect++
        					}
        				}
        				if(countCorrect == plugin.settings.correctAnswers[currentPage].length){
        					answeredOK = true;
        				} else {
        					answeredOK = false;
        				}
        			} else {
        				answeredOK = false;
        			}
        			
        			if(answeredOK){
        				markRightWrongAnswers('multiple', 'correct')
        			} else {
        				markRightWrongAnswers('multiple', 'wrong')
        			}
        		} break;
        	}
        }
        
        // mark the <li>s and question with correct / wrong classes
        var markRightWrongAnswers = function(type, status){
        	var liSelector = $(el + ' ' + plugin.settings.pagesIdentifiers[currentPage] + ' li')
        	if(type == 'single'){
        		liSelector.removeClass()
    			liSelector.eq(singleAnswer - 1).addClass(status)
    		} else {
    			liSelector.removeClass()
    			
    			for(var parse = 0; parse < multipleAnswers.length; parse++){
    				if(plugin.settings.correctAnswers[currentPage].indexOf(multipleAnswers[parse]) != -1){
    					liSelector.eq(multipleAnswers[parse] - 1).addClass('correct')
    				} else {
    					liSelector.eq(multipleAnswers[parse] - 1).addClass('wrong')
    				}
    			}
    		}
        	
        	if(status == 'correct'){
        		// if the answer is correct
        		$(el + ' ' + plugin.settings.questionIdentifier + ' .question_' + (currentPage+1))
					.removeClass('wrong')
					.addClass('correct')
					.find('.icon')
					.removeClass('wrong_icon question_icon')
					.addClass('correct_icon')
					.end()
        		
        		answeredOK = true;
        	} else {
        		// if the answer is wrong
        		$(el + ' ' + plugin.settings.questionIdentifier + ' .question_' + (currentPage+1))
					.removeClass('correct')
					.addClass('wrong')
					.find('.icon')
					.removeClass('correct_icon question_icon')
					.addClass('wrong_icon')
					.end()
				
				answeredOK = false;
        	}
        }
        
        // show the help / info page
        var displayHelpPage = function(){
        	$(el + ' .info_page')
        		//.slideDown(250)
        		.fadeIn(500)
        		.bind('click', function(){
        			$(this).fadeOut(250)
        		})
        }
        
        // call the constructor
        init();
    }
})(jQuery);