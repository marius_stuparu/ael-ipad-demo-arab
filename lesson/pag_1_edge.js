/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
var fonts = {
};
var symbols = {
"stage": {
   version: "0.1.4",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
      dom: [
        {
            id:'lichid-sus2',
            display:'none',
            className:'stage_lichid-sus2_id',
            type:'image',
            tag:'img',
            rect:[0,0,57,86],
            fill:['rgba(0,0,0,0)','images/lichid-sus.png'],
            transform:[[132.76488,111.16096],,,[1.17126,1.17126]]
        },
        {
            id:'constructie',
            className:'stage_constructie_id',
            type:'image',
            tag:'img',
            rect:[0,0,97,279],
            fill:['rgba(0,0,0,0)','images/constructie.png'],
            transform:[[112.61246,82.76254],,,[1.1211,1.1211]]
        },
        {
            id:'robinet-inchis',
            className:'stage_robinet-inchis_id',
            type:'image',
            tag:'img',
            rect:[0,0,16,24],
            fill:['rgba(0,0,0,0)','images/robinet-inchis.png'],
            transform:[[134.02631,196.23164],,,[1.0625,1.0625]]
        },
        {
            id:'robinet-deschis',
            display:'none',
            className:'stage_robinet-deschis_id',
            type:'image',
            tag:'img',
            rect:[0,0,17,14],
            fill:['rgba(0,0,0,0)','images/robinet-deschis.png'],
            transform:[[131.40864,201.83622],,,[0.78474,0.85269]]
        },
        {
            id:'substanta',
            className:'stage_substanta_id',
            type:'image',
            tag:'img',
            rect:[0,0,74,54],
            fill:['rgba(0,0,0,0)','images/substanta.png'],
            transform:[[124.85173,293.48993],,,[1.17087,1.17087]]
        },
        {
            id:'lichid-vas',
            className:'stage_lichid-vas_id',
            type:'image',
            tag:'img',
            rect:[0,0,79,54],
            clip:['rect(68px 79px 54px 0px)'],
            fill:['rgba(0,0,0,0)','images/lichid-vas.png'],
            transform:[[121.61253,293.56361],,,[1.00938,1.00939]]
        },
        {
            id:'stativ',
            className:'stage_stativ_id',
            type:'image',
            tag:'img',
            rect:[0,0,91,232],
            fill:['rgba(0,0,0,0)','images/stativ.png'],
            transform:[[296.82027,152]]
        },
        {
            id:'tub-jos',
            className:'stage_tub-jos_id',
            type:'image',
            tag:'img',
            rect:[0,0,177,132],
            fill:['rgba(0,0,0,0)','images/tub-jos.png'],
            transform:[[192.60738,260.73343],,,[1.0719,1.0719]]
        },
        {
            id:'lichid-eprubeta',
            className:'stage_lichid-eprubeta_id',
            type:'image',
            tag:'img',
            rect:[0,0,75,67],
            fill:['rgba(0,0,0,0)','images/lichid-eprubeta.png'],
            transform:[[342.57601,289.64938],[-0.96094]]
        },
        {
            id:'eprubeta',
            className:'stage_eprubeta_id',
            type:'image',
            rect:[0,0,17,153],
            opacity:0.4,
            fill:['rgba(0,0,0,0)','images/eprubeta.png'],
            transform:[[572.00001,191]]
        },
        {
            id:'tub-sus',
            display:'none',
            className:'stage_tub-sus_id',
            type:'image',
            tag:'img',
            rect:[0,0,173,108],
            fill:['rgba(0,0,0,0)','images/tub-sus.png'],
            transform:[[185.99998,187.51562]]
        },
        {
            id:'suport-eprubeta',
            className:'stage_suport-eprubeta_id',
            type:'image',
            tag:'img',
            rect:[0,0,99,117],
            fill:['rgba(0,0,0,0)','images/suport-eprubeta.png'],
            transform:[[532.02668,252.65096],,,[1.02088,1.02088]]
        },
        {
            id:'CaOH2',
            className:'stage_CaOH2_id',
            type:'text',
            rect:[483,430.29296875,161,63],
            text:"هيدروكسيد الكالسيوم (Ca(OH)2)",
            align:"center",
            font:["Arial Black, Gadget, sans-serif",14,"rgba(0,0,0,1)","normal","none",""],
            transform:[[17.5,-58.99998]]
        },
        {
            id:'CH3COOH',
            className:'stage_CH3COOH_id',
            type:'text',
            rect:[206.59719657898,62.499992370605,87,52.467628479004],
            text:"الخل (CH3COOH)",
            align:"center",
            font:["\'Arial Black\', Gadget, sans-serif",14,"rgba(0,0,0,1)","normal","none","normal"],
            transform:[[-188.00002,21.5]]
        },
        {
            id:'CaCO3',
            className:'stage_CaCO3_id',
            type:'text',
            rect:[14.467588424683,314.56478881836,82,75],
            text:"كربونات الكالسيوم (CaCO3)",
            align:"center",
            font:["\'Arial Black\', Gadget, sans-serif",14,"rgba(0,0,0,1)","normal","none","normal"],
            transform:[[6.5324,-13.2361]]
        },
        {
            id:'arrow_tube',
            className:'stage_arrow_tube_id',
            type:'rect',
            rect:[561,136,0,0]
        },
        {
            id:'arrow_drip',
            display:'none',
            className:'arrow_tubeCopy_id',
            type:'rect',
            rect:[561,136,0,0],
            transform:[[-486.41016,51]]
        },
        {
            id:'picatura',
            className:'stage_picatura_id',
            type:'rect',
            rect:[156.000003,260.00001,0,0]
        },
        {
            id:'bule-vas',
            className:'stage_bule-vas_id',
            type:'image',
            tag:'img',
            rect:[0,0,75,29],
            fill:['rgba(0,0,0,0)','images/bule-vas.png'],
            transform:[[122.99998,318],,,[0.86208,0.86208]]
        },
        {
            id:'bule_animate',
            className:'stage_bule_animate_id',
            type:'rect',
            rect:[410.000029,317.000012,0,0],
            transform:[[-11.72223,19.33332]]
        },
        {
            id:'inel',
            className:'stage_inel_id',
            type:'image',
            tag:'img',
            rect:[0,0,26,23],
            fill:['rgba(0,0,0,0)','images/inel.png'],
            transform:[[330,265]]
        }],
      symbolInstances: [
      {
         id:'arrow_tube',
         symbolName:'arrow'
      },
      {
         id:'arrow_drip',
         symbolName:'arrow'
      },
      {
         id:'picatura',
         symbolName:'picatura'
      },
      {
         id:'bule_animate',
         symbolName:'bule_animate'
      }
      ]
   },
   states: {
      "Base State": {
         "${_tub-sus}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '187.51562px'],
            ["transform", "translateX", '185.99998px']
         ],
         "${_CaOH2}": [
            ["style", "-webkit-transform-origin", [50,67],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-moz-transform-origin", [50,67],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [50,67],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [50,67],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [50,67],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "width", '161px'],
            ["style", "text-align", 'center'],
            ["style", "height", '63px'],
            ["style", "display", 'block'],
            ["transform", "translateX", '17.5px'],
            ["transform", "translateY", '-58.99998px'],
            ["style", "font-size", '14px']
         ],
         "${_CaCO3}": [
            ["transform", "translateX", '6.5324px'],
            ["style", "display", 'block'],
            ["style", "height", '75px'],
            ["transform", "translateY", '-13.2361px'],
            ["style", "width", '82px']
         ],
         "${_eprubeta}": [
            ["transform", "translateY", '191px'],
            ["style", "opacity", '0.4'],
            ["transform", "translateX", '572.00001px'],
            ["transform", "rotateZ", '0deg']
         ],
         "${_tub-jos}": [
            ["transform", "scaleY", '1.0719'],
            ["transform", "scaleX", '1.0719'],
            ["transform", "translateX", '192.60738px'],
            ["transform", "translateY", '260.73343px'],
            ["style", "display", 'block']
         ],
         "${_inel}": [
            ["transform", "translateX", '330px'],
            ["transform", "translateY", '265px']
         ],
         "${_lichid-eprubeta}": [
            ["transform", "translateX", '342.57601px'],
            ["transform", "rotateZ", '-0.96094deg'],
            ["style", "display", 'block'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '289.64938px']
         ],
         "${_suport-eprubeta}": [
            ["transform", "scaleX", '1.02088'],
            ["transform", "scaleY", '1.02088'],
            ["transform", "translateY", '252.65096px'],
            ["transform", "translateX", '532.02668px']
         ],
         "${_arrow_drip}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '51px'],
            ["transform", "translateX", '-486.41016px'],
            ["transform", "rotateZ", '-90deg']
         ],
         "${_bule-vas}": [
            ["transform", "translateX", '122.99998px'],
            ["transform", "scaleY", '0.86208'],
            ["style", "display", 'block'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '318px'],
            ["transform", "scaleX", '0.86208']
         ],
         "${_stage}": [
            ["style", "height", '575px'],
            ["style", "width", '700px'],
            ["color", "background-color", 'rgba(244,248,251,1.00)'],
            ["style", "overflow", 'hidden']
         ],
         "${_substanta}": [
            ["transform", "scaleY", '0.42592'],
            ["transform", "translateX", '124.10375px'],
            ["style", "display", 'block'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '306.721px'],
            ["transform", "scaleX", '1.06633']
         ],
         "${_CH3COOH}": [
            ["style", "display", 'block'],
            ["transform", "translateY", '21.5px'],
            ["transform", "translateX", '-188.00002px']
         ],
         "${_robinet-inchis}": [
            ["transform", "scaleY", '1.0625'],
            ["transform", "scaleX", '1.0625'],
            ["transform", "translateX", '134.02631px'],
            ["transform", "translateY", '196.23164px'],
            ["style", "display", 'block']
         ],
         "${_lichid-sus2}": [
            ["transform", "translateY", '111.16096px'],
            ["transform", "scaleY", '1.17126'],
            ["transform", "translateX", '132.59641px'],
            ["transform", "scaleX", '0.98987'],
            ["style", "opacity", '0.75'],
            ["style", "clip", [0,57,86,0],{valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}],
            ["style", "display", 'none']
         ],
         "${_picatura}": [
            ["style", "display", 'block'],
            ["style", "opacity", '0.5']
         ],
         "${_lichid-vas}": [
            ["transform", "translateX", '121.61253px'],
            ["transform", "scaleY", '1.00939'],
            ["transform", "scaleX", '1.00938'],
            ["transform", "translateY", '293.56361px'],
            ["style", "clip", [68,79,54,0],{valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}],
            ["style", "display", 'block']
         ],
         "${_constructie}": [
            ["transform", "scaleX", '1.1211'],
            ["transform", "scaleY", '1.1211'],
            ["transform", "translateY", '82.76254px'],
            ["transform", "translateX", '112.61246px']
         ],
         "${_robinet-deschis}": [
            ["transform", "translateX", '131.40864px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.85269'],
            ["transform", "translateY", '201.83622px'],
            ["transform", "scaleX", '0.78474']
         ],
         "${_stativ}": [
            ["style", "display", 'block'],
            ["transform", "translateY", '147px'],
            ["transform", "translateX", '296.82027px']
         ],
         "${_arrow_tube}": [
            ["style", "display", 'block']
         ],
         "${_bule_animate}": [
            ["style", "display", 'block'],
            ["transform", "translateY", '19.33332px'],
            ["transform", "translateX", '-11.72223px'],
            ["style", "-webkit-transform-origin", [54.6481,53.9274],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-moz-transform-origin", [54.6481,53.9274],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [54.6481,53.9274],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [54.6481,53.9274],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [54.6481,53.9274],{valueTemplate:'@@0@@% @@1@@%'}]
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 5999,
         autoPlay: true,
         labels: {

         },
         timeline: [
            { id: "eid455", tween: [ "style", "${_bule-vas}", "display", 'none', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid456", tween: [ "style", "${_bule-vas}", "display", 'block', { fromValue: 'none'}], position: 3927, duration: 0 },
            { id: "eid295", tween: [ "style", "${_arrow_tube}", "display", 'block', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid296", tween: [ "style", "${_arrow_tube}", "display", 'none', { fromValue: 'block'}], position: 60, duration: 0 },
            { id: "eid319", tween: [ "style", "${_arrow_drip}", "display", 'none', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid320", tween: [ "style", "${_arrow_drip}", "display", 'block', { fromValue: 'none'}], position: 2310, duration: 0 },
            { id: "eid368", tween: [ "style", "${_arrow_drip}", "display", 'none', { fromValue: 'block'}], position: 2500, duration: 0 },
            { id: "eid462", tween: [ "transform", "${_bule-vas}", "translateY", '318px', { fromValue: '318px'}], position: 3927, duration: 0 },
            { id: "eid463", tween: [ "transform", "${_bule-vas}", "translateY", '293.08504px', { fromValue: '318px'}], position: 4000, duration: 1000 },
            { id: "eid12", tween: [ "style", "${_robinet-inchis}", "display", 'block', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid397", tween: [ "style", "${_robinet-inchis}", "display", 'none', { fromValue: 'block'}], position: 2500, duration: 0 },
            { id: "eid381", tween: [ "style", "${_lichid-eprubeta}", "display", 'none', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid382", tween: [ "style", "${_lichid-eprubeta}", "display", 'block', { fromValue: 'none'}], position: 3138, duration: 0 },
            { id: "eid735", tween: [ "transform", "${_substanta}", "scaleY", '0.42592', { fromValue: '0.42592'}], position: 0, duration: 0 },
            { id: "eid363", tween: [ "style", "${_robinet-deschis}", "display", 'none', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid364", tween: [ "style", "${_robinet-deschis}", "display", 'block', { fromValue: 'none'}], position: 2500, duration: 0 },
            { id: "eid736", tween: [ "transform", "${_substanta}", "translateX", '124.10375px', { fromValue: '124.10375px'}], position: 0, duration: 0 },
            { id: "eid734", tween: [ "transform", "${_substanta}", "translateY", '306.721px', { fromValue: '306.721px'}], position: 0, duration: 0 },
            { id: "eid721", tween: [ "transform", "${_eprubeta}", "translateY", '224.3495px', { fromValue: '191px'}], position: 0, duration: 2000 },
            { id: "eid3", tween: [ "style", "${_CH3COOH}", "display", 'block', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid619", tween: [ "style", "${_bule_animate}", "display", 'none', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid620", tween: [ "style", "${_bule_animate}", "display", 'block', { fromValue: 'none'}], position: 3927, duration: 0 },
            { id: "eid739", tween: [ "transform", "${_stativ}", "translateY", '147px', { fromValue: '147px'}], position: 0, duration: 0 },
            { id: "eid722", tween: [ "transform", "${_eprubeta}", "rotateZ", '-44deg', { fromValue: '0deg'}], position: 0, duration: 2000 },
            { id: "eid461", tween: [ "transform", "${_bule-vas}", "translateX", '122.82798px', { fromValue: '122.99998px'}], position: 3927, duration: 0 },
            { id: "eid5", tween: [ "style", "${_lichid-sus2}", "display", 'block', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid726", tween: [ "transform", "${_lichid-sus2}", "scaleX", '0.98987', { fromValue: '0.98987'}], position: 0, duration: 0 },
            { id: "eid795", tween: [ "style", "${_lichid-sus2}", "opacity", '0.75', { fromValue: '0.75'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_CaCO3}", "display", 'block', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid373", tween: [ "style", "${_substanta}", "opacity", '0', { fromValue: '1'}], position: 3353, duration: 2646 },
            { id: "eid10", tween: [ "style", "${_lichid-vas}", "display", 'block', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid367", tween: [ "style", "${_lichid-vas}", "clip", [-13,79,54,0], { valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)', fromValue: [68,79,54,0]}], position: 2310, duration: 3311 },
            { id: "eid789", tween: [ "style", "${_picatura}", "opacity", '0.5', { fromValue: '0.5'}], position: 4500, duration: 0 },
            { id: "eid9", tween: [ "style", "${_stativ}", "display", 'block', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid366", tween: [ "style", "${_lichid-sus2}", "clip", [101,57,86,0], { valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)', fromValue: [0,57,86,0]}], position: 2310, duration: 3311 },
            { id: "eid13", tween: [ "style", "${_tub-jos}", "display", 'block', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid96", tween: [ "style", "${_tub-jos}", "display", 'none', { fromValue: 'block'}], position: 2250, duration: 0 },
            { id: "eid392", tween: [ "style", "${_lichid-eprubeta}", "opacity", '1.61', { fromValue: '0'}], position: 3175, duration: 574 },
            { id: "eid460", tween: [ "style", "${_bule-vas}", "opacity", '1', { fromValue: '0'}], position: 3927, duration: 455 },
            { id: "eid737", tween: [ "transform", "${_substanta}", "scaleX", '1.06633', { fromValue: '1.06633'}], position: 0, duration: 0 },
            { id: "eid4", tween: [ "style", "${_CaOH2}", "display", 'block', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid350", tween: [ "style", "${_CaOH2}", "display", 'none', { fromValue: 'block'}], position: 2310, duration: 0 },
            { id: "eid720", tween: [ "transform", "${_eprubeta}", "translateX", '355.86414px', { fromValue: '572.00001px'}], position: 0, duration: 2000 },
            { id: "eid11", tween: [ "style", "${_substanta}", "display", 'block', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid394", tween: [ "style", "${_picatura}", "display", 'none', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid395", tween: [ "style", "${_picatura}", "display", 'block', { fromValue: 'none'}], position: 2500, duration: 0 },
            { id: "eid414", tween: [ "style", "${_picatura}", "display", 'none', { fromValue: 'block'}], position: 5000, duration: 0 },
            { id: "eid725", tween: [ "transform", "${_lichid-sus2}", "translateX", '132.59641px', { fromValue: '132.59641px'}], position: 0, duration: 0 },
            { id: "eid1", tween: [ "style", "${_tub-sus}", "display", 'none', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid97", tween: [ "style", "${_tub-sus}", "display", 'block', { fromValue: 'none'}], position: 2250, duration: 0 }         ]
      }
   }
},
"arrow": {
   version: "0.1.4",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {"dom":[{"id":"arrow","className":"symbolStage_arrow_arrow_id","type":"image","tag":"img","rect":[0,0,28,35],"fill":["rgba(0,0,0,0)","images/arrow.png"],"transform":[[6,2]]}],"symbolInstances":[]},
   states: {
      "Base State": {
         "${_arrow}": [
            ["transform", "translateX", '6px'],
            ["transform", "translateY", '2.00462px']
         ],
         "${symbolSelector}": [
            ["style", "height", '40px'],
            ["style", "width", '40px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1000,
         autoPlay: true,
         labels: {

         },
         timeline: [
            { id: "eid706", tween: [ "transform", "${_arrow}", "translateY", '12px', { fromValue: '2.00462px'}], position: 0, duration: 250 },
            { id: "eid707", tween: [ "transform", "${_arrow}", "translateY", '-10px', { fromValue: '12px'}], position: 250, duration: 500 },
            { id: "eid708", tween: [ "transform", "${_arrow}", "translateY", '2px', { fromValue: '-10px'}], position: 750, duration: 250 },
            { id: "eid705", tween: [ "transform", "${_arrow}", "translateX", '6px', { fromValue: '6px'}], position: 0, duration: 0 }         ]
      }
   }
},
"picatura": {
   version: "0.1.4",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
       id:'picatura2',
       type:'image',
       rect:[0,0,8,21],
       opacity:0.75,
       fill:['rgba(0,0,0,0)','images/picatura2.png'],
       transform:[[2,-1],[0,0],[0],[1,1]]
   }],
   symbolInstances: [

   ]
},
   states: {
      "Base State": {
         "${_picatura2}": [
            ["style", "opacity", '0.75'],
            ["transform", "translateY", '-1px'],
            ["transform", "translateX", '2px']
         ],
         "${symbolSelector}": [
            ["style", "height", '15px'],
            ["style", "width", '11px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 500,
         autoPlay: false,
         labels: {

         },
         timeline: [
            { id: "eid784", tween: [ "transform", "${_picatura2}", "translateY", '47px', { fromValue: '-1px'}], position: 0, duration: 500 },
            { id: "eid780", tween: [ "transform", "${_picatura2}", "translateX", '2px', { fromValue: '2px'}], position: 0, duration: 0 },
            { id: "eid782", tween: [ "transform", "${_picatura2}", "translateX", '2px', { fromValue: '2px'}], position: 500, duration: 0 }         ]
      }
   }
},
"bule_eprubeta": {
   version: "0.1.4",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {"dom":[{"id":"bule-eprubeta","className":"symbolStage_bule_eprubeta_bule-eprubeta_id","type":"image","rect":[-402.197484,-334.999998,10,11],"fill":["rgba(0,0,0,0)","images/bule-eprubeta.svg"],"transform":[[402.0001,335.00008]]}],"symbolInstances":[]},
   states: {
      "Base State": {
         "${_bule-eprubeta}": [
            ["style", "-webkit-transform-origin", [52.5964,117.5636],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-moz-transform-origin", [52.5964,117.5636],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [52.5964,117.5636],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [52.5964,117.5636],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [52.5964,117.5636],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "opacity", '0'],
            ["transform", "translateY", '335.00008px'],
            ["transform", "translateX", '402.0001px']
         ],
         "${symbolSelector}": [
            ["style", "height", '11px'],
            ["style", "width", '10px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1000,
         autoPlay: true,
         labels: {

         },
         timeline: [
            { id: "eid470", tween: [ "transform", "${_bule-eprubeta}", "translateY", '285.01179px', { fromValue: '335.00008px'}], position: 0, duration: 1000, easing: "easeOutQuad" },
            { id: "eid469", tween: [ "transform", "${_bule-eprubeta}", "translateX", '349.30605px', { fromValue: '402.0001px'}], position: 0, duration: 1000, easing: "easeOutQuad" },
            { id: "eid472", tween: [ "style", "${_bule-eprubeta}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 500 }         ]
      }
   }
},
"bule_animate": {
   version: "0.1.4",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {"dom":[{"id":"bule-eprubeta3","type":"image","rect":[1.7360534667969,5.2083129882813,4,4],"fill":["rgba(0,0,0,0)","images/bule-eprubeta.png"],"transform":[[11,2],[0,0],[0],[1,1]]},{"id":"bule-eprubeta22","className":"symbolStage_bule_animate_bule-eprubeta22_id","type":"image","tag":"img","rect":[0,0,4,4],"fill":["rgba(0,0,0,0)","images/bule-eprubeta2.png"],"transform":[[10,15]]},{"id":"bule-eprubeta","type":"image","rect":[23.726806640625,8.6805419921875,4,4],"fill":["rgba(0,0,0,0)","images/bule-eprubeta.png"],"transform":[[-9,4],[0,0],[0],[1,1]]},{"id":"bule-eprubeta4","type":"image","rect":[16.203643798828,7.5231323242188,4,4],"fill":["rgba(0,0,0,0)","images/bule-eprubeta.png"],"transform":[[-1,0],[0,0],[0],[1,1]]},{"id":"bule-eprubeta5","type":"image","rect":[9.8379211425781,13.310180664063,4,4],"fill":["rgba(0,0,0,0)","images/bule-eprubeta.png"],"transform":[[1,1],[0,0],[0],[1,1]]}],"symbolInstances":[]},
   states: {
      "Base State": {
         "${_bule-eprubeta4}": [
            ["style", "opacity", '0.15'],
            ["transform", "translateY", '0'],
            ["transform", "translateX", '-1px']
         ],
         "${_bule-eprubeta}": [
            ["style", "-webkit-transform-origin", [39.582,61.8049],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-moz-transform-origin", [39.582,61.8049],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [39.582,61.8049],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [39.582,61.8049],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [39.582,61.8049],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "opacity", '0.15'],
            ["transform", "translateY", '4px'],
            ["transform", "translateX", '-9px']
         ],
         "${symbolSelector}": [
            ["style", "height", '22.000002px'],
            ["style", "width", '20.000002px']
         ],
         "${_bule-eprubeta3}": [
            ["style", "opacity", '0.15'],
            ["transform", "translateY", '2px'],
            ["transform", "translateX", '11px']
         ],
         "${_bule-eprubeta5}": [
            ["style", "opacity", '0.15'],
            ["transform", "translateX", '1px'],
            ["transform", "translateY", '1px']
         ],
         "${_bule-eprubeta22}": [
            ["style", "-webkit-transform-origin", [62.3634,79.6432],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-moz-transform-origin", [62.3634,79.6432],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [62.3634,79.6432],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [62.3634,79.6432],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [62.3634,79.6432],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "opacity", '0.15'],
            ["transform", "translateX", '10px'],
            ["transform", "translateY", '15px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1895.359769137,
         autoPlay: true,
         labels: {

         },
         timeline: [
            { id: "eid750", tween: [ "transform", "${_bule-eprubeta}", "translateX", '-67px', { fromValue: '-9px'}], position: 250, duration: 1000 },
            { id: "eid775", tween: [ "style", "${_bule-eprubeta5}", "opacity", '1', { fromValue: '0.15000000596046448'}], position: 708, duration: 1186 },
            { id: "eid760", tween: [ "transform", "${_bule-eprubeta3}", "translateY", '-50px', { fromValue: '2px'}], position: 500, duration: 1000 },
            { id: "eid751", tween: [ "transform", "${_bule-eprubeta}", "translateY", '-52px', { fromValue: '4px'}], position: 250, duration: 1000 },
            { id: "eid752", tween: [ "style", "${_bule-eprubeta}", "-webkit-transform-origin", [39.582,61.8049], { valueTemplate: '@@0@@% @@1@@%', fromValue: [39.582,61.8049]}], position: 1250, duration: 0 },
            { id: "eid796", tween: [ "style", "${_bule-eprubeta}", "-moz-transform-origin", [39.582,61.8049], { valueTemplate: '@@0@@% @@1@@%', fromValue: [39.582,61.8049]}], position: 1250, duration: 0 },
            { id: "eid797", tween: [ "style", "${_bule-eprubeta}", "-ms-transform-origin", [39.582,61.8049], { valueTemplate: '@@0@@% @@1@@%', fromValue: [39.582,61.8049]}], position: 1250, duration: 0 },
            { id: "eid798", tween: [ "style", "${_bule-eprubeta}", "msTransformOrigin", [39.582,61.8049], { valueTemplate: '@@0@@% @@1@@%', fromValue: [39.582,61.8049]}], position: 1250, duration: 0 },
            { id: "eid799", tween: [ "style", "${_bule-eprubeta}", "-o-transform-origin", [39.582,61.8049], { valueTemplate: '@@0@@% @@1@@%', fromValue: [39.582,61.8049]}], position: 1250, duration: 0 },
            { id: "eid744", tween: [ "transform", "${_bule-eprubeta22}", "translateX", '-50.18518px', { fromValue: '10px'}], position: 0, duration: 1000 },
            { id: "eid759", tween: [ "transform", "${_bule-eprubeta3}", "translateX", '-41px', { fromValue: '11px'}], position: 500, duration: 1000 },
            { id: "eid767", tween: [ "transform", "${_bule-eprubeta4}", "translateY", '-55px', { fromValue: '0px'}], position: 708, duration: 991 },
            { id: "eid746", tween: [ "style", "${_bule-eprubeta22}", "opacity", '1', { fromValue: '0.15000000596046448'}], position: 0, duration: 1000 },
            { id: "eid745", tween: [ "transform", "${_bule-eprubeta22}", "translateY", '-43.4491px', { fromValue: '15px'}], position: 0, duration: 1000 },
            { id: "eid768", tween: [ "style", "${_bule-eprubeta4}", "opacity", '1', { fromValue: '0.15000000596046448'}], position: 708, duration: 991 },
            { id: "eid761", tween: [ "style", "${_bule-eprubeta3}", "opacity", '1', { fromValue: '0.15000000596046448'}], position: 500, duration: 1000 },
            { id: "eid766", tween: [ "transform", "${_bule-eprubeta4}", "translateX", '-53px', { fromValue: '-1px'}], position: 708, duration: 991 },
            { id: "eid774", tween: [ "transform", "${_bule-eprubeta5}", "translateY", '-54px', { fromValue: '1px'}], position: 708, duration: 1186 },
            { id: "eid773", tween: [ "transform", "${_bule-eprubeta5}", "translateX", '-56px', { fromValue: '1px'}], position: 708, duration: 1186 },
            { id: "eid754", tween: [ "style", "${_bule-eprubeta}", "opacity", '1', { fromValue: '0.15000000596046448'}], position: 250, duration: 1000 }         ]
      }
   }
}
};

Edge.registerCompositionDefn(compId, symbols, fonts);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "pag_1");
