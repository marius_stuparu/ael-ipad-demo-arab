/***********************
* Adobe Edge Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

//Edge symbol: 'stage'
(function(symbolName) {


Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
sym.setParameter("step", "0");

parent.$(parent.document).find('.details').html('قم بتثبيت الأنبوبة في المكان المناسب على الحامل وشاهد عرض الوسائط المتعددة وسجل ملاحظاتك.').end()

sym.stop();

});
//Edge binding end

Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
sym.stop();

});
//Edge binding end






Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2310.8984375, function(sym, e) {
sym.setParameter("step", "1");

parent.$(parent.document).find('.details').html('اضغط على القمع لإضافة القليل من الخل إلى القارورة.').end()

sym.stop();

});
//Edge binding end


Symbol.bindElementAction(compId, symbolName, "${_robinet-inchis}", "click", function(sym, e) {
step = sym.getParameter("step");

if(step == "1"){
	sym.play();
}

});
//Edge binding end

Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2500, function(sym, e) {
sym.getSymbol("picatura").play();

});
//Edge binding end

Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3927.36328125, function(sym, e) {
sym.getSymbol("bule_animate").play();

});
//Edge binding end

Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 5000, function(sym, e) {
$("#quiz").show();
console.warn('mod');
launchQuiz();

});
//Edge binding end











Symbol.bindElementAction(compId, symbolName, "${_eprubeta}", "touchstart", function(sym, e) {
step = sym.getParameter("step");

if(step == "0"){
	sym.play();
}

});
//Edge binding end

Symbol.bindElementAction(compId, symbolName, "${_eprubeta}", "click", function(sym, e) {
step = sym.getParameter("step");

if(step == "0"){
	sym.play();
}

});
//Edge binding end





})("stage");
//Edge symbol end:'stage'

//=========================================================

//Edge symbol: 'arrow'
(function(symbolName) {
Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
// play the timeline from the given position (ms or label)
sym.play(0);

});
//Edge binding end
















})("arrow");
//Edge symbol end:'arrow'

//=========================================================

//Edge symbol: 'picatura'
(function(symbolName) {
Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 500, function(sym, e) {
sym.play(0);

});
//Edge binding end















})("picatura");
//Edge symbol end:'picatura'

//=========================================================

//Edge symbol: 'bule_eprubeta'
(function(symbolName) {














})("bule_eprubeta");
//Edge symbol end:'bule_eprubeta'

//=========================================================

//Edge symbol: 'bule_animate'
(function(symbolName) {
Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
sym.stop();

});
//Edge binding end

Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1895.359769137, function(sym, e) {
sym.stop();

});
//Edge binding end















})("bule_animate");
//Edge symbol end:'bule_animate'

})(jQuery, AdobeEdge, "pag_1");